<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    

    public function submit(Request $request)
    {
        $article = Article::create([
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'author' => $request->input('author'),
            'body' => $request->input('body')
        ]);

        // return redirect()->back()->with('status', 'Articolo inviato con successo');
        return redirect(route('home'))->with('status', 'Articolo inviato con successo');
    }


    public function create()
    {
        return view('article.form');
    }

}