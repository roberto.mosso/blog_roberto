<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
    //  * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $articles = Article::all()->sortDesc()->take(3);
        // dd($articles);


        return view('home', compact('articles'));
    }


    public function articleIndex()
    {
        $articles = Article::all()->sortDesc();
        // dd($articles);

        return view('article.index', compact('articles'));
    }

    
    public function articleShow(Article $article)
    {
        // con la dependency injection (inserendo che $article fa parte della classe Article) non c'è più bisogno del codice successivo:

        // $result = Article::find($article);

        // dd($result);


        return view('article.show', compact('article'));
    }


    public function contacts()
    {
        return view('contacts');
    }

    
    public function mailSubmit(ContactRequest $request)
    {
        // dd($request->all());

        $username = $request->input('username');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $message = $request->input('message');

        $contact = compact('username', 'message');

        Mail::to($email)->send(new ContactMail($contact));

        return redirect()->back()->status('status', 'Il tuo messaggio è stato inviato');
    }

    // public function thankyou()
    // {
    //     return view('thank-you');
    // }
}
