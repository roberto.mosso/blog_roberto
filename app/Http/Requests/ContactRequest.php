<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:3|max:30',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required|min:3|max:10',
            'message' => 'required|min:10|max:150'
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username is required',
            'username.min' => 'Username is too short',
            'username.max' => 'Username is too long',


        ];
    }
}
