  <x-layout>
  
  
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/contact-bg.jpg')">
    <div class="overlay bg-filter"></div>
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8 offset-lg-2 col-md-10 offset-md-1 mx-auto">
          <div class="page-heading">

            @if (session('status'))
            <div class="container mb-5">
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3 alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
            @endif

            <h1>Contatti</h1>
            {{-- <span class="subheading">Hai domande? Scrivimi</span> --}}
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
          
        <p class="h3">Ci teniamo in contatto? Mandami un messaggio e ti ricontatterò il prima possibile!</p>
        
        <form class="my-4" name="sentMessage" id="contactForm" novalidate method="POST" action="{{route('mail.submit')}}">
        @csrf
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Nome</label>
              <input type="text" class="form-control" name="name" placeholder="Nome" id="name" required data-validation-required-message="Inserisci il tuo nome">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Indirizzo email</label>
              <input type="email" class="form-control" name="email" placeholder="Email" id="email" required data-validation-required-message="Inserisci il tuo indirizzo email">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
              <label>Telefono</label>
              <input type="tel" class="form-control" name="phone" placeholder="Telefono" id="phone" required data-validation-required-message="Inserisci il tuo numero di telefono">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Messaggio</label>
              <textarea rows="5" class="form-control" name="message" placeholder="Messaggio" id="message" required data-validation-required-message="Scrivi qui il tuo messaggio"></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>

          {{-- <div id="success"></div> --}}

         
          <button type="submit" class="btn btn-primary my-4 float-right" id="sendMessageButton">Invia &rarr;</button>
             
         
          {{-- <a href="{{route('home')}}"><button class="btn btn-primary  my-5">Torna alla homepage &rarr;</button></a> --}}
            
          
          
        </form>
      </div>
    </div>
  </div>

  </x-layout>