<x-layout>


<!-- Page Header -->
  <header class="masthead" style="background-image: url('img/about-bg.jpg')">
    <div class="overlay "></div>
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-8 offset-md-2">
          <div class="site-heading">

            @if (session('status'))
            <div class="container mb-5">
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3 alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
            @endif

            <h1 class="text-center">The Blog</h1>
            <span class="subheading text-center">Il blog che mancava</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">

    <div class="row">
      <div class="col-12 d-flex justify-content-center">
        <h2 class="mt-3 mb-5 text-center">Gli ultimi articoli pubblicati:</h2>
      </div>
    </div>
      
      
    <div class="row mt-3">
      
      <div class="col-12 col-lg-8 mx-auto">
        @foreach ($articles as $article)
        <div class="post-preview">
          <a href="{{route('article.show', $article)}}">
          
            <h2 class="post-title">
              {{$article->title}}
            </h2>
            <h3 class="post-subtitle">
              {{$article->subtitle}}
            </h3>
          </a>
          <p class="post-meta">Pubblicato da <a href="#">{{$article->author}}</a> il {{$article->created_at->format('d-m-Y')}}</p>
        </div>
        <hr>


      @endforeach

      <div class="clearfix">
        <a class="btn btn-primary float-right my-5" href="{{route('article.index')}}">Tutti gli articoli &rarr;</a>
      </div>
    
    
    </div>  

    
  </div>



</x-layout>
