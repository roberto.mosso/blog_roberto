<x-layout>

<!-- Page Header -->
  <header class="masthead" style="background-image: url('img/about-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">

            <h1>Archivio articoli</h1>
            {{-- <span class="subheading">Il blog che mancava</span> --}}
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      
      
      {{-- {{dd($article)}}; --}}
      
      
      <div class="col-12 col-lg-8 mx-auto">
        @foreach ($articles as $article)
        <div class="post-preview">
          <a href="{{route('article.show', $article)}}">
            <h2 class="post-title">
              {{$article->title}}
            </h2>
            <h3 class="post-subtitle">
              {{$article->subtitle}}
            </h3>
          </a>
          <p class="post-meta">Pubblicato da <a href="#">{{$article->author}}</a> il {{$article->created_at->format('d-m-Y')}}</p>
        </div>
        <hr>


      @endforeach


          <a href="{{route('home')}}"><button class="btn btn-primary float-right my-5">Torna alla homepage &rarr;</button></a>

    </div>
  </div>




</x-layout>