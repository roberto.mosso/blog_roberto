<x-layout>


  
  
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-12 mx-auto">
          <div class="page-heading">
            
            {{-- @if (session('status'))
            <div class="container mb-5">
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3 alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
            @endif --}}

            <h1>Inserisci il tuo articolo</h1>
            
          </div>
        </div>
      </div>
    </div>
  </header>





  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
  
        <form class="my-4" name="sentMessage" id="contactForm" novalidate method="POST" action="{{route('article.submit')}}">
          @csrf
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Titolo</label>
              <input type="text" class="form-control" name="title" placeholder="Titolo" id="name" required data-validation-required-message="Inserisci il titolo dell'articolo">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Sottotitolo</label>
              <input type="text" class="form-control" name="subtitle" placeholder="Sottotitolo" id="name" required data-validation-required-message="Inserisci il sottotitolo dell'articolo">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Autore</label>
              <input type="text" class="form-control" name="author" placeholder="Autore" id="email" required data-validation-required-message="Inserisci l'autore">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Articolo</label>
              <textarea rows="5" class="form-control" name="body" placeholder="Articolo" id="message" required data-validation-required-message="Inserisci il tuo testo"></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>
          <div id="success"></div>
          <button type="submit" class="btn btn-primary my-4 float-right" id="sendMessageButton">Invia &rarr;</button>
          {{-- <a href="{{route('home')}}"><button class="btn btn-primary float-right mt-5">Torna alla homepage &rarr;</button></a> --}}

        </form>

      </div>
    </div>
  </div>

  </x-layout>