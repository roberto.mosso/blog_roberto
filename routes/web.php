<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/articoli', [HomeController::class, 'articleIndex'])->name('article.index');

Route::get('/articoli/{article}', [HomeController::class, 'articleShow'])->name('article.show');

Route::get('/contatti', [HomeController::class, 'contacts'])->name('contacts');

Route::post('/contatti/submit', [HomeController::class, 'mailSubmit'])->name('mail.submit');




Route::get('/form-articolo', [ArticleController::class, 'create'])->name('article.form');

Route::post('/article/submit', [ArticleController::class, 'submit'])->name('article.submit');


